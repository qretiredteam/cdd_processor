






* Steps to create a new Module.

1. Create a scala class in com.quintiles.cdd.main which extends Module trait.
2. Override execute method. 
3. Write loadHdfsData method following CDDModule. This method should load datasets only if the application has not loaded the data sets earlier.
4. Add the module mapping in PostScoop.getModule() method.   
5. The directory for adding sql files for a module is module name inside resources. In run time you can provide different sqls by give 
   a new directory path in the properties. The format for property is : <MODULE_NAME>_SQL  
6. Any number of sql files can be put in and they would be executed one at a time. The file name has two parts A__B.sql ,
    A= The sqls are sorted based on this value and are executed in the sorted order., B = the output table name for this sql.
    ex : a file name 3__observation.slq will out put the data as observation and run this file 3rd in order.
             
7. Write Test module to test the module functionality. This can be based of ObservationModuleTest.
8.  All the input data for testing can be put in <module_name>/data/ and sql files in <module_name>/sql/

      