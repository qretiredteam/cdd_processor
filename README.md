# README #

Getting set up with CDD_PROCESSOR 

### What is this repository for? ###

* Quick summary : Reads data from OMOP data and out puts Fixed width CDD data.

* Version : 1.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up  :  Download this repo and execute sbt compile. To package and deploy sbt assembly

* Configuration : 
* Dependencies : in SBT
* Database configuration : NA
* How to run tests : sbt test
* Deployment instructions :  nohup  ./bin/spark-submit --verbose --class com.quintiles.cdd.main.PostScoop  --master yarn-client  --num-executors 8 --driver-memory 50g --executor-memory 50g --executor-cores 4 --files ../scripts/log4j.properties   /u02/app/webadm/scripts/cdd_processor.jar  /u02/app/webadm/CDD/cdd.properties /u02/app/webadm/CDD/sql.properties &
* To Run Tests : To run tests locally, Add "-Xmx13g -XX:MaxPermSize=512m" to the VM Parameters for the test cases. 
    To run from command line : 
        Step 1: export SBT_OPTS="-Xmx13g -XX:MaxPermSize=512m -Dlog4j.debug"
        Step 2: sbt test  (To run all tests) 
        Step 3: to run only one test  :: sbt "test-only com.quintiles.cdd.main.ObservationModuleTest"
        

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact