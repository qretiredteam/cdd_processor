import _root_.sbtassembly.AssemblyPlugin.autoImport._
import _root_.sbtassembly.PathList

name := "CCDPostScoop"

version := "1.0"

scalaVersion := "2.10.4"

//scalacOptions += "-target:jvm-1.7"

javacOptions ++= Seq("-source", "1.7", "-target", "1.7")

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.4.1" //% "provided"

//libraryDependencies += "org.apache.spark" %% "spark-core" % "1.4.1" % "test"


libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.4.1" //% "provided"

//libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.4.1" % "test"

libraryDependencies += "com.databricks" % "spark-csv_2.10" % "1.2.0"

libraryDependencies += "org.glassfish.jersey.core" % "jersey-server" % "2.13" % "test"

libraryDependencies += "org.apache.hive" % "hive-jdbc" % "1.2.1" //% "provided"

//libraryDependencies += "org.apache.hive" % "hive-jdbc" % "1.2.1" % "test"

libraryDependencies += "org.apache.spark" % "spark-hive_2.10" % "1.4.1"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.2.4" % "test"


libraryDependencies ++= Seq("org.slf4j" % "slf4j-api" % "1.7.6",
  "org.slf4j" % "slf4j-simple" % "1.7.6",
  "org.clapper" %% "grizzled-slf4j" % "1.0.1")

resolvers += "justwrote" at "http://repo.justwrote.it/releases/"

resolvers += "Restlet Repository" at "http://maven.restlet.org"

//libraryDependencies += "it.justwrote" %% "scala-faker" % "0.3"
//libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.2.1" % "test"

mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) => {
  case PathList("javax", "pom.properties", xs@_*) => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".class" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".RSA" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith "mailcap" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".xml" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".dtd" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".default" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".xsd" => MergeStrategy.first
  case "pom.properties" => MergeStrategy.first
  case "unwanted.txt" => MergeStrategy.discard
  case x => old(x)
}
}

test in assembly := {}

assemblyExcludedJars in assembly := {
  val cp = (fullClasspath in assembly).value
  cp filter {_.data.getName == "spark-assembly-1.4.1-hadoop2.4.0.jar"}
}


assemblyJarName in assembly := "cdd_processor.jar"


    