SELECT c.concept_id
                                   FROM omopConcept c
                                        INNER JOIN omopVocabulary v
                                                ON c.vocabulary_id = v.vocabulary_id
                                  WHERE v.vocabulary_name = 'Observation'
                                    AND c.concept_code IN ('Lab','Vital','Miscellaneous')