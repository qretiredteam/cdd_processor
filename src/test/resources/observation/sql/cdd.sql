 SELECT
             padString(obsf.tracer_id),
             padString(obsf.patient_key),
             padString(obsf.observation_key),
             padString(obsf.obs_value),
             padString(obsf.unit_observation_key),
             padString(obsf.create_date),
             padString(obsf.obs_date),
             padString(obsf.low_normal),
             padString(obsf.high_normal),
             padString(obsf.activity_tracer_id),
             padString(obsf.age_key)
                 FROM   cddObservationFJoined obsf
             INNER JOIN cddBasePersonCohort pc
                     ON (obsf.patient_key = pc.patient_key)
        WHERE (pc.phr_flag = 'Y')

