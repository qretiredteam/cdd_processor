package com.quintiles.cdd.main

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by dhiraj on 9/28/15.
 */
trait   BaseTestModule  {

  def setup(): SparkObj = {
    val conf = new SparkConf()
      .setMaster("local[3]")
      .setAppName("ObservationModuleTest_test")
      .set("spark.driver.memory", "10g")
      .set("spark.ui.enabled", "false")
    val sc = new SparkContext(conf)

//    val hiveContext = new org.apache.spark.sql.hive.HiveContext(sc)
    val sqlContext = new SQLContext(sc)
    SparkObj(sc, sqlContext)

  }

  def close(spark: SparkObj) = {
    spark.sc.stop()
  }


}
