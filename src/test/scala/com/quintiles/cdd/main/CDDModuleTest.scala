package com.quintiles.cdd.main

import java.io.File
import java.nio.file.Paths

import com.quintiles.cdd.main.PostScoop.DataFilePath
import org.scalatest.FunSuite

/**
 * Created by dhiraj on 9/29/15.
 */
class CDDModuleTest extends FunSuite with BaseTestModule {

  test("CDDModule test") {
    val m = new CDDModule()
    val tempOutputPath = Paths.get(this.getClass().getResource(".").toURI()).toAbsolutePath.toString + "/cdd_path_temp"
    println("tempOutputPath  : " + tempOutputPath)
    m.delete(new File(tempOutputPath))

    val sqlContentMap: List[(String, String)] = m.getSqlListContent("/cdd/sql/")
    val spark = setup()
    val stringToString: Map[String, String] = Map(Constants.SAVE_DATA -> Constants.SAVE_DATA,
      Constants.CDD_OBSERVATIONF_JOINED_TABLE + "_DELIMITER" -> Constants.COMMA.toString,
      Constants.OUTPUT_PATH -> (tempOutputPath + "/cdd_output/")
    )
    spark.sqlContext.udf.register("padString10", (s: String) => Option(s).getOrElse("").replaceAll("\"", "").padTo(10, " ").mkString.substring(0, 10))

    val observationFPath = Util.unZipIt(Paths.get(this.getClass().getResource("/common_data/observationF_actual.zip").toURI()).toAbsolutePath.toString, tempOutputPath)

    val cddBasePersonCohortPath = Util.unZipIt(Paths.get(this.getClass().getResource("/observation/data/base_person_cohort.zip").toURI()).toAbsolutePath.toString, tempOutputPath)

    val dfp = DataFilePath(
      omopVocabulary = null,
      omopConcept = null,
      omopObservation = null,
      cddPatientD = null,
      cddActivityF = null,
      cddBasePersonCohort = cddBasePersonCohortPath,
      cddObservationF = observationFPath
    )


    val output = m.execute(spark.sc, spark.sqlContext, dfp, stringToString, sqlContentMap)
    // output.df.repartition(1).write.format("com.databricks.spark.csv").option("header", "true").save(tempOutputPath+"/cdd")
    println(" cdd outout is  " + output.df.count() + " in " + tempOutputPath)
    close(spark)
  }



}
