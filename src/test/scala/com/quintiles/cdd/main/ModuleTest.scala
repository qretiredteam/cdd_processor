package com.quintiles.cdd.main

import java.nio.file.Paths

import com.quintiles.cdd.main.PostScoop.DataFilePath
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.scalatest.FunSuite


/**
 * Created by dhiraj on 9/29/15.
 */
class ModuleTest extends FunSuite {


  class Dummy extends Module {
    override def execute(sc: SparkContext, hiveContext: SQLContext, dfp: DataFilePath, props: Map[String, String], sqlContent: List[(String, String)]): ModuleOutput = {
      null
    }
  }


  test(" test  ") {
    val d = new Dummy()
    val res = d.getSqlList(Paths.get(this.getClass().getResource("/observation/sql/").toURI()).toAbsolutePath.toString)
    assert(res.keys.toList == List(1, 2))
    assert(res.values.toMap.keys.toList == List("conceptId", "observationf"))
    assert(res.size == 2)
  }


}
