package com.quintiles.cdd.main

import java.io._
import java.util.zip.{ZipEntry, ZipInputStream}

/**
 * Created by dhiraj on 9/24/15.
 */
object Util {


  def validateFiles(): Unit = {

  }


  def unZipIt(zipFile: String, outputFolder: String): String = {

    val buffer = new Array[Byte](1024)
    var fileName: String = null
    try {

      //output directory
      val folder = new File(outputFolder)
      if (!folder.exists()) {
        folder.mkdir()
      }

      //zip file content
      val zis: ZipInputStream = new ZipInputStream(new FileInputStream(zipFile))
      //get the zipped file list entry
      var ze: ZipEntry = zis.getNextEntry()
      if(ze == null)
        throw new FileNotFoundException("No file found")
      while (ze != null) {

        fileName = ze.getName()
        if(fileName == null)
          throw new FileNotFoundException("name cannot be null")
        val newFile = new File(outputFolder + File.separator + fileName)

        System.out.println("file unzip : " + newFile.getAbsoluteFile())

        //create folders
        new File(newFile.getParent()).mkdirs()
        val fos = new FileOutputStream(newFile)
        var len: Int = zis.read(buffer)
        while (len > 0) {
          fos.write(buffer, 0, len)
          len = zis.read(buffer)
        }

        fos.close()
        ze = zis.getNextEntry()
      }

      zis.closeEntry()
      zis.close()

    } catch {
      case e: IOException => println("exception caught: " + e.getMessage)
    }

    outputFolder + "/" + fileName
  }


}
