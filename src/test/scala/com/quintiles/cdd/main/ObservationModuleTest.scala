package com.quintiles.cdd.main

import java.io.File
import java.nio.file.Paths

import org.apache.spark.SparkContext
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SQLContext
import org.scalatest.FunSuite

import com.quintiles.cdd.main.PostScoop.DataFilePath


/**
 * Created by dhiraj on 9/18/15.
 */

case class TransferObjTest(mapProperties: Map[String, String], sqlListContent: List[String])

case class SparkObj(sc: SparkContext, sqlContext: SQLContext)

class ObservationModuleTest extends FunSuite with BaseTestModule {


  test("compare two RDDs") {

  }

  test("test ObservationModule") {

    val m = new ObservationModule()
    val tempOutputPath = Paths.get(this.getClass().getResource(".").toURI()).toAbsolutePath.toString+File.separator+"observation_path_temp"

    println("tempOutputPath  : " + tempOutputPath)

    m.delete(new File(tempOutputPath))

    println("observation"+File.separator+"sql"+File.separator)
    val sqlContentMap: List[(String, String)] = m.getSqlListContent("/observation/sql")


    val spark = setup()

    val observationPath = Util.unZipIt(Paths.get(this.getClass().getResource("/observation/data/observation.zip").toURI()).toAbsolutePath.toString, tempOutputPath)
    val omopConceptPath = Util.unZipIt(Paths.get(this.getClass().getResource("/observation/data/concept.zip").toURI()).toAbsolutePath.toString, tempOutputPath)
    val omopVocabularyPath = Util.unZipIt(Paths.get(this.getClass().getResource("/observation/data/vocabulary.zip").toURI()).toAbsolutePath.toString, tempOutputPath)
    val cddPatientDPath = Util.unZipIt(Paths.get(this.getClass().getResource("/observation/data/patient_d.zip").toURI()).toAbsolutePath.toString, tempOutputPath)
    val cddBasePersonCohortPath = Util.unZipIt(Paths.get(this.getClass().getResource("/observation/data/base_person_cohort.zip").toURI()).toAbsolutePath.toString, tempOutputPath)
    val cddActivityFPath = Util.unZipIt(Paths.get(this.getClass().getResource("/observation/data/activity.zip").toURI()).toAbsolutePath.toString, tempOutputPath)

    val observationFPath = Util.unZipIt(Paths.get(this.getClass().getResource("/common_data/observationF_actual.zip").toURI()).toAbsolutePath.toString, tempOutputPath)

    val dfp = DataFilePath(omopVocabulary = omopVocabularyPath,
      omopConcept = omopConceptPath,
      omopObservation = observationPath,
      cddPatientD = cddPatientDPath,
      cddActivityF = cddActivityFPath,
      cddBasePersonCohort = cddBasePersonCohortPath,
      cddObservationF = observationFPath
    )

    val stringToString: Map[String, String] = Map(Constants.SAVE_DATA -> Constants.SAVE_DATA, Constants.OUTPUT_PATH -> (tempOutputPath + File.separator+"test_observation_output"))

    val output = m.execute(spark.sc, spark.sqlContext, dfp, stringToString, sqlContentMap)


    // This is special logic to filter out null only for testing hopefully...
    val ndf = output.df.filter("obs_value != ''")
    println(" filtered result is  "+ndf.count())


    println(" count is  :: " + ndf + " temp res is  " + stringToString.getOrElse(Constants.OUTPUT_PATH, ""))

    val actualRes = dfp.cddObservationF
    val actualDF = spark.sqlContext.read.format("com.databricks.spark.csv").option("header", "true").load(actualRes)
    val actualDFSel =   actualDF.select("patient_key", "observation_key", "obs_value", "unit_observation_key", "low_normal", "high_normal", "cqr_organization_id", "cqr_person_id", "cqr_observation_id", "cqr_visit_occurrence_id")
    val generatedSel = ndf.select("patient_key", "observation_key", "obs_value", "unit_observation_key", "low_normal", "high_normal", "cqr_organization_id", "cqr_person_id", "cqr_observation_id", "cqr_visit_occurrence_id")
    val diffDF:DataFrame = actualDFSel.except(generatedSel)

    //val joined = actualDFSel.intersect(generatedSel)
    val actual_count = actualDFSel.count()
    val gen_count = generatedSel.count()

    println("  The patient keys which are same are  number of elements which are same in both. and actual_count is : "+actual_count+" and gen count is :  "+gen_count)

    assert(diffDF.count() == 0 )
    assert(gen_count == actual_count)

//    println("actual patient_key :"+actualDFSel.select("patient_key").distinct.collect().toList)
//    println("GEnerated patient_key :"+generatedSel.select("patient_key").distinct.collect().toList)
//    println("diff patient_key :"+diffDF.select("patient_key").distinct.collect().toList)


//    val diffPath = stringToString.getOrElse(Constants.OUTPUT_PATH, "") + "_diff"
//    val actualPath = stringToString.getOrElse(Constants.OUTPUT_PATH, "") + "_actual"
//    val genPath = stringToString.getOrElse(Constants.OUTPUT_PATH, "") + "_gen"


//    diffDF.repartition(1).write.format("com.databricks.spark.csv").option("header", "true").save(diffPath)
//    actualDFSel.repartition(1).write.format("com.databricks.spark.csv").option("header", "true").save(actualPath)
//    generatedSel.repartition(1).write.format("com.databricks.spark.csv").option("header", "true").save(genPath)
//
//    println(" written log to  " + diffPath)
    close(spark)
  }


}
