package com.quintiles.cdd.main

import org.scalatest.FunSuite

/**
 * Created by dhiraj on 9/29/15.
 */
class MySampleTest extends FunSuite with BaseTestModule {

  test("CDDModule test") {
    val spark = setup()
    val cddObservationF = spark.sqlContext.read.format("com.databricks.spark.csv").options(Map("header" -> "true", "delimiter" -> ",","quote"->"\001"  )).load("/Users/dhiraj/Desktop/CDD.csv")
    cddObservationF.count()
    cddObservationF.write.format("com.databricks.spark.csv").options(Map("quote"->"")).save("/Users/dhiraj/Desktop/delme/CDD_o.csv")
    close(spark)
  }



}
