package com.quintiles.cdd

import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by dhiraj on 10/7/15.
 */
object SampleTest {

  def main(args: Array[String]) {
    val conf = if (args.length == 0) {
      new SparkConf()
        .setMaster("local[3]")
        .setAppName("Omop_Pre")
        .set("spark.driver.memory", "13g")

    } else {
      new SparkConf()
    }

    val sc = new SparkContext(conf)
    //val sqlContext = new SQLContext(sc)
    val sqlContext = new org.apache.spark.sql.hive.HiveContext(sc)
    val cddObservationF = sqlContext.read.format("com.databricks.spark.csv").options(Map("header" -> "true", "delimiter" -> ",","quote"->"\001"  )).load("/Users/dhiraj/Desktop/CDD.csv")
    cddObservationF.count()
    cddObservationF.write.format("com.databricks.spark.csv").save("/Users/dhiraj/Desktop/CDD_o.csv")

  }
}
