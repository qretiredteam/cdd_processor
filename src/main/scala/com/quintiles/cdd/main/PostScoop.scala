package com.quintiles.cdd.main

import java.util.Properties
import javax.naming.NameNotFoundException

import com.quintiles.cdd.util.BaseUtil
import grizzled.slf4j.Logging
import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source._

/**
 * Created by dhiraj on 7/27/15.
 */

object PostScoop extends Logging {


  case class DataFilePath(omopVocabulary: String, omopConcept: String, omopObservation: String, cddBasePersonCohort: String, cddPatientD: String, cddActivityF: String, cddObservationF: String)

  //case class DataTables(omopVocabulary: DataFrame, omopConcept: DataFrame, omopObservation: DataFrame, cddBasePersonCohort: DataFrame, cddPatientD: DataFrame, cddActivityF: DataFrame, cddObservationF: DataFrame)

  def main(args: Array[String]) {

    val props = BaseUtil.loadProperties(args)

    val sqlBeginTime = System.currentTimeMillis()

    val myTempSql1 = fromFile(args(4)).getLines().reduceLeft(_ + _)
    val observationFSql = fromFile(args(1)).getLines().reduceLeft(_ + _)
    val cddSql = fromFile(args(2)).getLines().reduceLeft(_ + _)

    val dfp: DataFilePath = loadProps(props)

    val outputHDFSFilePath = props.getProperty(Constants.OUTPUT_PATH)
    //val outputLocalMergeFilePath = props.getProperty("OUTPUT_LOCAL_MERGE_PATH")
    //val copyAndMerge = if (props.getProperty("COPY_AND_MERGE", "true") == "true") true else false
    import collection.JavaConverters._
    val scalaMap: Map[String, String] = props.asScala.toMap

    val conf = if (args.length == 0) {
      new SparkConf()
        .setMaster("local[3]")
        .setAppName("Omop_Pre")
        .set("spark.driver.memory", "13g")

    } else {
      new SparkConf()
    }

    val sc = new SparkContext(conf)
    //val sqlContext = new SQLContext(sc)
    val sqlContext = new org.apache.spark.sql.hive.HiveContext(sc)
    sqlContext.udf.register("padString10", (s: String) => Option(s).getOrElse("").replaceAll("\"", "").padTo(10, " ").mkString.substring(0, 10))
    sqlContext.udf.register("padString8", (s: String) => Option(s).getOrElse("").replaceAll("\"", "").padTo(8, " ").mkString.substring(0, 8))
    sqlContext.udf.register("padString12", (s: String) => Option(s).getOrElse("").replaceAll("\"", "").padTo(12, " ").mkString.substring(0, 12))

    // get the list of modules to be executed.
    val modulesList = props.getProperty("CDD_MODULES", "observationF").split(",").map(a => getModule(a))

    // Executing the modules.
    modulesList.foreach(a => {
      a.execute(sc, sqlContext, dfp, scalaMap, null)
    })
  }


  /**
   * When new modules are added, they need to be put in this list.
   * @param str
   * @return
   */
  def getModule(str: String): Module = {

    str match {
      case Constants.OBSERVATIONF_MODULE_NAME => new ObservationModule()
      case Constants.CDD_MODULE_NAME => new CDDModule()
      case _ => throw new NameNotFoundException("Invalid Name")
    }

  }


  def loadProps(props: Properties): DataFilePath = {
    val omopVocabularyPath = props.getProperty("OMOP_VOCABULARY_PATH")
    val omopConceptPath = props.getProperty("OMOP_CONCEPT")
    val omopObservationPath = props.getProperty("OMOP_OBSERVATION")
    val cddBasePersonCohortPath = props.getProperty("CDD_BASE_PERSON_COHORT")
    val cddPatientDPath = props.getProperty("CDD_PATIENTID")
    val cddActivityFPath = props.getProperty("CDD_ACTIVITY")
    val cddObservationFPath = props.getProperty("CDD_OBSERVATIONF")
    val dfp = DataFilePath(omopVocabulary = omopVocabularyPath,
      omopConcept = omopConceptPath,
      omopObservation = omopObservationPath,
      cddPatientD = cddPatientDPath,
      cddActivityF = cddActivityFPath,
      cddBasePersonCohort = cddBasePersonCohortPath,
      cddObservationF = cddObservationFPath
    )
    dfp
  }

  def printDetails(): Unit = {
    logger.debug(" +tables.cddActivityF.count() " + GlobalDataFrameVO.cddActivityF.count())
    logger.trace(" sample cddActivityF  " + GlobalDataFrameVO.cddActivityF.take(5).toList)

    logger.debug(" +tables.cddBasePersonCohort.count() " + GlobalDataFrameVO.cddBasePersonCohort.count())
    logger.trace(" sample cddBasePersonCohort  " + GlobalDataFrameVO.cddBasePersonCohort.take(5).toList)

    logger.debug(" +tables.cddObservationF.count() " + GlobalDataFrameVO.cddObservationF.count())
    logger.trace(" sample cddObservationF  " + GlobalDataFrameVO.cddObservationF.take(5).toList)

    logger.debug(" +tables.cddPatientD.count() " + GlobalDataFrameVO.cddPatientD.count())
    logger.trace(" sample cddPatientD  " + GlobalDataFrameVO.cddPatientD.take(5).toList)

    logger.debug(" +tables.omopConcept.count() " + GlobalDataFrameVO.omopConcept.count())
    logger.trace(" sample omopConcept  " + GlobalDataFrameVO.omopConcept.take(5).toList)

    logger.debug(" +tables.omopObservation.count() " + GlobalDataFrameVO.omopObservation.count())
    logger.trace(" sample omopObservation  " + GlobalDataFrameVO.omopObservation.take(5).toList)

    logger.debug(" +tables.omopVocabulary.count() " + GlobalDataFrameVO.omopVocabulary.count())
    logger.trace(" sample omopVocabulary  " + GlobalDataFrameVO.omopVocabulary.take(5).toList)
  }


  def loadHiveData(sc: SQLContext, basePerson: String, observations: String): String = {
    null
  }


}
