package com.quintiles.cdd.main

import com.quintiles.cdd.main.PostScoop._
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SQLContext}
import com.quintiles.cdd.traits._
import java.io.File

/**
 * Created by dhiraj on 9/17/15.
 */


object Constants {

  val DELIMITER_DEFAULT: Char = '\001'
  val COMMA: Char = ','

  val OBSERVATIONF_MODULE_NAME = "observationF"
  val CDD_MODULE_NAME = "cdd"

  val myTempSql1 = "observation_1_sql"
  val patientObservationTempTable = "patientObservationTempTable"
  val observationFSql = "observationFSql"
  val OUTPUT_PATH = "OUTPUT_HDFS_FILE_PATH"

  val SAVE_DATA = "SAVE_DATA"

  val CDDACTIVITYF_TABLE = "cddActivityF"
  val OMOP_VOCABULARY_TABLE: String = "omopVocabulary"
  val OMOP_CONCEPT_TABLE: String = "omopConcept"
  val OMOP_OBSERVATION_TABLE: String = "omopObservation"
  val CDD_BASE_PERSON_COHORT_TABLE: String = "cddBasePersonCohort"
  val CDDPATIENTD_TABLE: String = "cddPatientD"
  val CDD_OBSERVATIONF_JOINED_TABLE: String = "cddObservationFJoined"

  val cddObservationFJoined = "cddObservationFJoined"

}

class ObservationModule extends Module with ObservationModuleTrait 
  with ActivityModuleTrait with PatientDModuleTrait 
  with BasePersonCohortModuleTrait with OMOPObservationModuleTrait
  with OMOPConceptModuleTrait with OMOPVocabularyModuleTrait{


  override def execute(sc: SparkContext, hiveContext: SQLContext, dfp: DataFilePath, props: Map[String, String], sqlContent: List[(String, String)]): ModuleOutput = {

    println("ENTER EXECUTE")
    
    val sqlContentMap: List[(String, String)] = if (sqlContent == null) getSqlListContent(File.separator+"observation"+File.separator+"sql"+File.separator) else sqlContent
    loadHdfsData(sc, hiveContext, dfp)
    processSqls(sc, hiveContext, dfp, props, sqlContentMap)
  }


  /**
   * Load all the tables required to create CDD data.
   *
   * @param sc
   * @param sql
   * @param tablesPath
   * @return
   */
  def loadHdfsData(sc: SparkContext, sql: SQLContext, tablesPath: DataFilePath) = {

    val schemeConf = new SchemaConf(sc, sql, tablesPath)
    val omopVocabulary: DataFrame = omopVocabularyFunc(schemeConf)
    val omopConcept: DataFrame = omopConceptSchema(schemeConf)
    val omopObservation: DataFrame = omopObservationFunc_(schemeConf)
    val cddBasePersonCohort: DataFrame = cddBasePersonCohortFunc_(schemeConf)
    val cddPatientD: DataFrame = patientDFunc_(schemeConf)
    val cddActivityF: DataFrame = activityFunc_(schemeConf)
    val cddObservationF: DataFrame = observationFSchemaFunc_(schemeConf)
    
  }

}
