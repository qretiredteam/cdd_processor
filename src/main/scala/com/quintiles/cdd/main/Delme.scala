package com.quintiles.cdd.main


import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by dhiraj on 8/6/15.
 */
object Delme {


  def main(args: Array[String]) {
    val conf =
      new SparkConf()
        .setMaster("local[3]")
        .setAppName("Omop_Pre")
        .set("spark.driver.memory", "13g")

    val sc = new SparkContext(conf)
    val sql = new org.apache.spark.sql.SQLContext(sc)

    val omopVocabularySchema = StructType(Array(
      StructField("vocabulary_id",StringType,false),
      StructField("vocabulary_name",StringType,false),
      StructField("vocabulary_code",StringType,false)))

    val myRdd = sc.textFile("/Users/dhiraj/Documents/my_experiment/CCDPostScoop/src/main/resources/test_csv.csv")
    val df = sql.createDataFrame(myRdd.map(a => Row.fromSeq(a.split(',').toSeq)),omopVocabularySchema)
    df.map(a => a.toSeq.toArray.mkString(",").replaceAll("\"","")).saveAsTextFile("/Users/dhiraj/Documents/my_experiment/CCDPostScoop/src/main/resources/_output")



    //println(res(0).toString)


//    val format = CSVFormat.DEFAULT.withDelimiter('\001').withIgnoreEmptyLines(true);
//    val parser = CSVParser.parse(new File("/Users/dhiraj/Documents/my_experiment/CCDPostScoop/src/main/resources/sample_data.csv"), Charset.forName("UTF-8"),format)
//    val records = parser.getRecords
//    val vm = records.get(1).iterator()

    //println(v)
//    val omopVocabulary = sql.read.format("com.databricks.spark.csv").schema(omopVocabularySchema).options(Map("header" -> "false", "delimiter" -> "\001"  )).load(tablesPath.omopVocabulary)

  }
}
