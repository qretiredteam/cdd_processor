package com.quintiles.cdd.main

import com.quintiles.cdd.main.PostScoop._
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SQLContext}
import com.quintiles.cdd.traits.ObservationModuleTrait
import com.quintiles.cdd.traits.BasePersonCohortModuleTrait

/**
 * Created by dhiraj on 9/17/15.
 */


class CDDModule extends Module with ObservationModuleTrait with BasePersonCohortModuleTrait{


  override def execute(sc: SparkContext, hiveContext: SQLContext, dfp: DataFilePath, props: Map[String, String], sqlContent: List[(String, String)]): ModuleOutput = {

    val sqlContentMap: List[(String, String)] = if (sqlContent == null) getSqlListContent("/cdd/sql/") else sqlContent
    loadHdfsData(sc, hiveContext, dfp,props)

     processSqls(sc, hiveContext, dfp, props, sqlContentMap)
  }




  /**
   * Load all the tables required to create CDD data.
   *
   * @param sc
   * @param sql
   * @param tablesPath
   * @return
   */
  private def loadHdfsData(sc: SparkContext, sql: SQLContext, tablesPath: DataFilePath,props: Map[String, String]) = {
  val schemeConf = new SchemaConf(sc, sql, tablesPath)
    if (GlobalDataFrameVO.cddObservationF == null) {
      
      val delimiter:Char = props.getOrElse(Constants.CDD_OBSERVATIONF_JOINED_TABLE+"_DELIMITER",Constants.DELIMITER_DEFAULT).toString.toCharArray.last
      val observationF: DataFrame = observationFSchemaFunc_(schemeConf)
      observationF.registerTempTable(Constants.CDD_OBSERVATIONF_JOINED_TABLE)
      GlobalDataFrameVO.cddObservationF = observationF
      println(GlobalDataFrameVO.cddObservationF.count())
    }
    if (GlobalDataFrameVO.cddBasePersonCohort == null) {
      val delimiter:Char = props.getOrElse(Constants.CDD_BASE_PERSON_COHORT_TABLE+"_DELIMITER",Constants.DELIMITER_DEFAULT).toString.toCharArray.last
      val cddBasePersonCohort: DataFrame = cddBasePersonCohortFunc_(schemeConf)
      cddBasePersonCohort.registerTempTable(Constants.CDD_BASE_PERSON_COHORT_TABLE)
      GlobalDataFrameVO.cddBasePersonCohort = cddBasePersonCohort
      println(GlobalDataFrameVO.cddBasePersonCohort.count())
    }

  }

}
