package com.quintiles.cdd.main

import java.io.File
import java.nio.file.Paths

import com.quintiles.cdd.main.PostScoop.DataFilePath
import grizzled.slf4j.Logging
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Row, SQLContext}

import scala.collection.immutable.ListMap
import scala.io.Source._

/**
 * Created by dhiraj on 9/17/15.
 */

object ModuleConstants {

  val DELIMITER_DEFAULT_CHAR: Char = '\001'
  val DELIMITER_DEFAULT_STR: String = "\001"
  val COMMA_CHAR: Char = ','
  val COMMA_STR: String = ","
  val TRUE_STR="true"
  val FALSE_STR ="false";
  val HEADER ="header"
  val DELIMITER = "delimiter"
  val QUOTE = "quote"
  
  val OPTION_MAP = Map(HEADER -> TRUE_STR, DELIMITER -> COMMA_STR ,QUOTE->DELIMITER_DEFAULT_STR )
  
  val DATABRICKS_CSV_FORMAT="com.databricks.spark.csv"
  val CDD_MODULE_NAME = "cdd"
  val OUTPUT_PATH = "OUTPUT_HDFS_FILE_PATH"
  val SAVE_DATA = "SAVE_DATA"

}
case class ModuleOutput(df: DataFrame)

object GlobalDataFrameVO {
  var omopVocabulary: DataFrame = null
  var omopConcept: DataFrame = null
  var omopObservation: DataFrame = null
  var cddBasePersonCohort: DataFrame = null
  var cddPatientD: DataFrame = null
  var cddActivityF: DataFrame = null
  var cddObservationF: DataFrame = null
}


trait Module extends Logging {
  


  def execute(sc: SparkContext, hql: SQLContext, dfp: DataFilePath, props: Map[String, String], sqlContent: List[(String, String)]): ModuleOutput


  def getSqlListContent(path: String): List[(String, String)] = {
    
    
    val sqlListMap = getSqlList(Paths.get(this.getClass().getResource(path).toURI).toAbsolutePath.toString).values.toMap
    //val sqlListMap = getSqlList(Paths.get(path).toAbsolutePath.toString).values.toMap
    val sqlContentMap: List[(String, String)] = sqlListMap.map(a => (a._1, fromFile(a._2).getLines().reduceLeft(_ + _))).toList
    sqlContentMap
  }

  /**
   *
   * @param sqlDir
   * @return the file names in sorted order of the number before "_" and the value is a map of file name after "_" and the absolute file path
   *         ex: 1_conceptID.sql => (1->(conceptId -> /../resources/observation/sql/1_conceptId.sql)
   */
  def getSqlList(sqlDir: String): ListMap[Int, (String, String)] = {
    
    val list = getListOfFiles(sqlDir)
    
    val map = list.map(a => (a.getName.split("__"), a.getAbsolutePath)).map(a => (a._1(0).toInt, (a._1(1).substring(0, a._1(1).indexOf(".")), a._2)))
    
    ListMap(map.toSeq.sortBy(_._1): _*)
    
  }

  private def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).filter(a => a.getName.contains("__")).filter(a => a.getAbsolutePath.endsWith(".sql")).toList
    } else {
      List[File]()
    }
  }

  def delete(file: File) {
    if (file.isDirectory)
      Option(file.listFiles).map(_.toList).getOrElse(Nil).foreach(delete(_))
    file.delete
  }

  def processSqls(sc: SparkContext, SQLContext: SQLContext, dfp: DataFilePath, props: Map[String, String], sqlContent: List[(String, String)]): ModuleOutput = {
    var myTempSql1DF: DataFrame = null
    var finalSqlName: String = null

    sqlContent.foreach(a => {
      val startTime = System.currentTimeMillis()
      myTempSql1DF = SQLContext.sql(a._2)
      myTempSql1DF.registerTempTable(a._1)
      val count = myTempSql1DF.count()
      val diffTime = (System.currentTimeMillis() - startTime)/1000

      logger.info(" records in data  " + a._1 + "  are :" + count)
      logger.info(s" time taken for sql ${a._1} is  ${diffTime} ")

      finalSqlName = a._1
    })


    val res: DataFrame = myTempSql1DF //SQLContext.sql(props.getOrElse(Constants.observationFSql, ""))

    if (logger.isDebugEnabled || props.getOrElse(Constants.SAVE_DATA, ModuleConstants.FALSE_STR) == Constants.SAVE_DATA) {
      val a = if (props.getOrElse(Constants.SAVE_DATA, ModuleConstants.FALSE_STR) == Constants.SAVE_DATA) {
        // only enable this for testing locally.
        val ares = res.coalesce(1)
        //delete(new File(props.getOrElse(Constants.OUTPUT_PATH, "")))
        //Files.deleteIfExists(java.nio.file.Paths.get(props.getOrElse(Constants.observationF_output,"")))
        ares
      } else res

      a.write.format(ModuleConstants.DATABRICKS_CSV_FORMAT).option(ModuleConstants.HEADER, ModuleConstants.TRUE_STR).save(props.getOrElse(Constants.OUTPUT_PATH, "") + "/" + finalSqlName + "_1")
    }

    ModuleOutput(res)
  }
  
  
  def createDateFrame(rdd: RDD[String], omopVocabularySchema: StructType, sql: SQLContext, delimiter: Char): DataFrame =
    sql.createDataFrame(rdd.map(a => a.replaceAll("\"", "")).map(a => Row.fromSeq(a.split(delimiter).toSeq)), omopVocabularySchema)

    
    

}
