package com.quintiles.cdd.com.quintiles.cdd.hadoop.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;


import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by cloudera on 6/10/15.
 */
public class HadoopFileUtil {




    public static void mergeHdfsDirToLocalFile(String sourceFile, String distFile) throws Exception{
        Configuration configuration = new Configuration();
        FileSystem hdfs = FileSystem.get(new URI(sourceFile), configuration);
        FileSystem localFileSystem = FileSystem.getLocal(configuration);
        org.apache.hadoop.fs.FileUtil.copyMerge(hdfs, new Path(sourceFile),localFileSystem,new Path(distFile),false,configuration,"");


    }

    /**
     *
      * @param zip output zip file
     * @param file Existing text file
     * @throws IOException
     */
    public static void zip(File zip, File file, boolean deleteOriginial) throws IOException {
        ZipOutputStream zos = null;
        try {
            String name = file.getName();
            zos = new ZipOutputStream(new FileOutputStream(zip));

            ZipEntry entry = new ZipEntry(name);
            zos.putNextEntry(entry);

            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
                byte[] byteBuffer = new byte[1024];
                int bytesRead = -1;
                while ((bytesRead = fis.read(byteBuffer)) != -1) {
                    zos.write(byteBuffer, 0, bytesRead);
                }
                zos.flush();
            } finally {
                try {
                    fis.close();
                } catch (Exception e) {
                }
            }
            zos.closeEntry();

            zos.flush();
            if(deleteOriginial){
                file.delete();
            }
        } finally {
            try {
                zos.close();
            } catch (Exception e) {
            }
        }
    }


    public static void main(String[] args) {
        try {
            mergeHdfsDirToLocalFile(args[0],args[1]);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}