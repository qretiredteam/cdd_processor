package com.quintiles.cdd.util

import java.io.{FileReader, BufferedReader}
import java.util.Properties

/**
 * Created by dhiraj on 8/4/15.
 */
object BaseUtil {

  /**
   *
   * @param filePath
   * @return
   */
  def loadProperties(filePath: Array[String]): Properties = {
    val t = new Properties()
    t.load(new BufferedReader(new FileReader(filePath(0))))
    if (filePath.length > 1) {
      val p = new Properties()
      p.load(new BufferedReader(new FileReader(filePath(1))))
      t.putAll(p)
    }
    t
  }


}
