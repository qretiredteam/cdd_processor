package com.quintiles.cdd.traits

import com.quintiles.cdd.main.PostScoop.DataFilePath
import grizzled.slf4j.Logging
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import java.io.File
import java.nio.file.Paths
import scala.io.Source._
import com.quintiles.cdd.main.Module
import com.quintiles.cdd.main._

trait OMOPConceptModuleTrait extends SchemaTrait{
  def omopConceptSchema(contextArgs: SchemaConf): DataFrame = {
     val omopConceptSchema = StructType(Array(
      StructField("concept_id", StringType, false),
      StructField("vocabulary_id", StringType, false),
      StructField("concept_level", StringType, false),
      StructField("concept_class", StringType, false),
      StructField("concept_code", StringType, false),
      StructField("concept_name", StringType, false),
      StructField("concept_desc", StringType, false),
      StructField("valid_start_date", StringType, false),
      StructField("valid_end_date", StringType, false),
      StructField("invalid_reason", StringType, false),
      StructField("comments", StringType, false),
      StructField("created_date", StringType, false),
      StructField("created_by", StringType, false),
      StructField("last_modified_date", StringType, false),
      StructField("last_modified_by", StringType, false)


    ))

    GlobalDataFrameVO.omopConcept = createSchemaDF(Constants.OMOP_CONCEPT_TABLE,omopConceptSchema, contextArgs, contextArgs.tablesPath.omopConcept)
    GlobalDataFrameVO.omopConcept
    
   }
}