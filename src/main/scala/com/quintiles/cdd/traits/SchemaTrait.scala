package com.quintiles.cdd.traits

import com.quintiles.cdd.main.ModuleConstants
import com.quintiles.cdd.main.PostScoop.DataFilePath
import org.apache.spark.SparkContext
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SQLContext}

trait SchemaTrait {
   class SchemaConf(val sc: SparkContext, val sql: SQLContext, val tablesPath: DataFilePath)
   
    def createSchemaDF(jointTable:String,fsSchema: StructType, conf: SchemaConf, tablesPath: String): DataFrame = {
     val cddObjectF = conf.sql.read.format(ModuleConstants.DATABRICKS_CSV_FORMAT).schema(fsSchema).options(ModuleConstants.OPTION_MAP).load(tablesPath)
    cddObjectF.registerTempTable(jointTable)
    cddObjectF.count()
    cddObjectF
  }
}