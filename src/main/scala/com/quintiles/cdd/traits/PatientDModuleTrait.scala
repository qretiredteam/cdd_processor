package com.quintiles.cdd.traits

import com.quintiles.cdd.main.PostScoop.DataFilePath
import grizzled.slf4j.Logging
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import java.io.File
import java.nio.file.Paths
import scala.io.Source._
import com.quintiles.cdd.main.Module
import com.quintiles.cdd.main._

trait PatientDModuleTrait extends SchemaTrait{
  def patientDFunc_(contextArgs: SchemaConf): DataFrame = {
    val cddPatientDSchema = StructType(Array(

      StructField("patient_key", StringType, false),
      StructField("gender", StringType, false),
      StructField("race", StringType, false),
      StructField("age_key", StringType, false),
      StructField("home_location_zip", StringType, false),
      StructField("home_location_state", StringType, false),
      StructField("home_location_division", StringType, false),
      StructField("home_location_region", StringType, false),
      StructField("first_pmo_date", StringType, false),
      StructField("last_pmo_date", StringType, false),
      StructField("first_doc_activity_date", StringType, false),
      StructField("last_doc_activity_date", StringType, false),
      StructField("insurance_payment_type", StringType, false),
      StructField("patient_status", StringType, false),
      StructField("study_cohort_group_indicator", StringType, false),
      StructField("specialty_key", StringType, false),
      StructField("is_idn_flag", StringType, false),
      StructField("study_key", StringType, false),
      StructField("batch_request_id", StringType, false),
      StructField("cqr_organization_id", StringType, false),
      StructField("cqr_person_id", StringType, false),
      StructField("cqr_provider_id", StringType, false),
      StructField("dob_year_month", StringType, false),
      StructField("created_date", StringType, false),
      StructField("created_by", StringType, false),
      StructField("last_modified_date", StringType, false),
      StructField("last_modified_by", StringType, false)
    ))
    GlobalDataFrameVO.cddPatientD = createSchemaDF(Constants.CDDPATIENTD_TABLE,cddPatientDSchema, contextArgs, contextArgs.tablesPath.cddPatientD)
    GlobalDataFrameVO.cddPatientD
    
   }
}