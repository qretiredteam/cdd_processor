package com.quintiles.cdd.traits

import com.quintiles.cdd.main.PostScoop.DataFilePath
import grizzled.slf4j.Logging
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import java.io.File
import java.nio.file.Paths
import scala.io.Source._
import com.quintiles.cdd.main.Module
import com.quintiles.cdd.main._

trait OMOPVocabularyModuleTrait extends SchemaTrait{
  def omopVocabularyFunc(contextArgs: SchemaConf): DataFrame = {
    val omopVocabularySchema = StructType(Array(
      StructField("vocabulary_id", StringType, false),
      StructField("vocabulary_name", StringType, false),
      StructField("vocabulary_code", StringType, false),
      StructField("created_date", StringType, false),
      StructField("created_by", StringType, false),
      StructField("last_modified_date", StringType, false),
      StructField("last_modified_by", StringType, false),
      StructField("comments", StringType, false)

    ))

    GlobalDataFrameVO.omopVocabulary = createSchemaDF(Constants.OMOP_VOCABULARY_TABLE,omopVocabularySchema, contextArgs, contextArgs.tablesPath.omopVocabulary)
    GlobalDataFrameVO.omopVocabulary
    
   }
}