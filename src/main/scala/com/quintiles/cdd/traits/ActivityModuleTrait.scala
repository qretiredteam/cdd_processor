package com.quintiles.cdd.traits

import com.quintiles.cdd.main.PostScoop.DataFilePath
import grizzled.slf4j.Logging
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import java.io.File
import java.nio.file.Paths
import scala.io.Source._
import com.quintiles.cdd.main.Module
import com.quintiles.cdd.main._

trait ActivityModuleTrait extends SchemaTrait{
   def activityFunc_(contextArgs: SchemaConf): DataFrame = {
      val cddActivityFSchema = StructType(Array(
      StructField("study_key", StringType, false),
      StructField("tracer_id", StringType, false),
      StructField("patient_key", StringType, false),
      StructField("activity_type", StringType, false),
      StructField("create_date", StringType, false),
      StructField("activity_date", StringType, false),
      StructField("activity_type_desc", StringType, false),
      StructField("age_key", StringType, false),
      StructField("specialty_key", StringType, false),
      StructField("batch_request_id", StringType, false),
      StructField("cqr_organization_id", StringType, false),
      StructField("cqr_person_id", StringType, false),
      StructField("cqr_visit_occurrence_id", StringType, false),
      StructField("created_date", StringType, false),
      StructField("created_by", StringType, false),
      StructField("last_modified_date", StringType, false),
      StructField("last_modified_by", StringType, false)
    ))
    GlobalDataFrameVO.cddActivityF = createSchemaDF(Constants.CDDACTIVITYF_TABLE,cddActivityFSchema, contextArgs, contextArgs.tablesPath.cddActivityF)
    GlobalDataFrameVO.cddActivityF
    
   }
}