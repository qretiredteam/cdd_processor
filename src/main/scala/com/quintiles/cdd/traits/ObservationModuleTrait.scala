package com.quintiles.cdd.traits

import com.quintiles.cdd.main.PostScoop.DataFilePath
import grizzled.slf4j.Logging
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import java.io.File
import java.nio.file.Paths
import scala.io.Source._
import com.quintiles.cdd.main.Module
import com.quintiles.cdd.main._


trait ObservationModuleTrait extends SchemaTrait{
  
  def observationFSchemaFunc_(contextArgs: SchemaConf): DataFrame = {
    val cddObservationFSchema = StructType(Array(
      StructField("patient_key", StringType, false),
      StructField("tracer_id", StringType, false),
      StructField("observation_key", StringType, false),
      StructField("value_quality", StringType, false),
      StructField("obs_value", StringType, false),
      StructField("unit_observation_key", StringType, false),
      StructField("create_date", StringType, false),
      StructField("obs_date", StringType, false),
      StructField("low_normal", StringType, false),
      StructField("high_normal", StringType, false),
      StructField("activity_tracer_id", StringType, false),
      StructField("age_key", StringType, false),
      StructField("study_key", StringType, false),
      StructField("batch_request_id", StringType, false),
      StructField("cqr_organization_id", StringType, false),
      StructField("cqr_person_id", StringType, false),
      StructField("cqr_observation_id", StringType, false),
      StructField("cqr_visit_occurrence_id", StringType, false),
      StructField("created_date", StringType, false),
      StructField("created_by", StringType, false),
      StructField("last_modified_date", StringType, false),
      StructField("last_modified_by", StringType, false)
    ))
     GlobalDataFrameVO.cddObservationF = createSchemaDF(Constants.CDD_OBSERVATIONF_JOINED_TABLE,cddObservationFSchema, contextArgs, contextArgs.tablesPath.cddObservationF)
     GlobalDataFrameVO.cddObservationF
  }
}