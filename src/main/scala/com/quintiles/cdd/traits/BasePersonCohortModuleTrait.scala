package com.quintiles.cdd.traits

import com.quintiles.cdd.main.PostScoop.DataFilePath
import grizzled.slf4j.Logging
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import java.io.File
import java.nio.file.Paths
import scala.io.Source._
import com.quintiles.cdd.main.Module
import com.quintiles.cdd.main._

trait BasePersonCohortModuleTrait extends SchemaTrait{
  def cddBasePersonCohortFunc_(contextArgs: SchemaConf): DataFrame = {
    val cddBasePersonCohortSchema = StructType(Array(

      StructField("patient_key", StringType, false),
      StructField("asz_flag", StringType, false),
      StructField("pfi_flag", StringType, false),
      StructField("phr_flag", StringType, false),
      StructField("san_flag", StringType, false),
      StructField("tru_flag", StringType, false),
      StructField("tru_skinny_flag", StringType, false),
      StructField("patient_age_key", StringType, false),
      StructField("dob_year_month", StringType, false),
      StructField("created_date", StringType, false),
      StructField("created_by", StringType, false),
      StructField("last_modified_date", StringType, false),
      StructField("last_modified_by", StringType, false)

    ))

    GlobalDataFrameVO.cddBasePersonCohort = createSchemaDF(Constants.CDD_BASE_PERSON_COHORT_TABLE,cddBasePersonCohortSchema, contextArgs, contextArgs.tablesPath.cddBasePersonCohort)
    GlobalDataFrameVO.cddBasePersonCohort
    
   }
}