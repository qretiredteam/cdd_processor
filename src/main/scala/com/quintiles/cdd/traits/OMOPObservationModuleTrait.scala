package com.quintiles.cdd.traits

import com.quintiles.cdd.main.PostScoop.DataFilePath
import grizzled.slf4j.Logging
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import java.io.File
import java.nio.file.Paths
import scala.io.Source._
import com.quintiles.cdd.main.Module
import com.quintiles.cdd.main._

trait OMOPObservationModuleTrait extends SchemaTrait{
  def omopObservationFunc_(contextArgs: SchemaConf): DataFrame = {
    val omopObservationSchema = StructType(Array(
      StructField("organization_id", StringType, false),
      StructField("person_id", StringType, false),
      StructField("observation_id", StringType, false),
      StructField("observation_concept_id", StringType, false),
      StructField("observation_date", StringType, false),
      StructField("observation_time", StringType, false),
      StructField("value_as_number", StringType, false),
      StructField("value_as_", StringType, false),
      StructField("value_as_concept_id", StringType, false),
      StructField("unit_concept_id", StringType, false),
      StructField("range_low", StringType, false),
      StructField("range_high", StringType, false),
      StructField("observation_type_concept_id", StringType, false),
      StructField("associated_provider_id", StringType, false),
      StructField("visit_occurrence_id", StringType, false),
      StructField("relevant_condition_concept_id", StringType, false),
      StructField("observation_source_value", StringType, false),
      StructField("units_source_value", StringType, false),
      StructField("created_date", StringType, false),
      StructField("created_by", StringType, false),
      StructField("last_modified_date", StringType, false),
      StructField("last_modified_by", StringType, false),
      StructField("comments", StringType, false),
      StructField("source_key", StringType, false),
      StructField("partition_key", StringType, false),
      StructField("observation_source_string", StringType, false)

    ))

    GlobalDataFrameVO.omopObservation = createSchemaDF(Constants.OMOP_OBSERVATION_TABLE,omopObservationSchema, contextArgs, contextArgs.tablesPath.omopObservation)
    GlobalDataFrameVO.omopObservation
    
   }
}