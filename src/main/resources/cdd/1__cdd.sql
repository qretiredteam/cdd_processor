 SELECT
             padString10(obsf.tracer_id),
             padString10(obsf.patient_key),
             padString10(obsf.observation_key),
             padString10(obsf.obs_value),
             padString10(obsf.unit_observation_key),
             padString10(obsf.create_date),
             padString10(obsf.obs_date),
             padString10(obsf.low_normal),
             padString10(obsf.high_normal),
             padString10(obsf.activity_tracer_id),
             padString10(obsf.age_key)
                 FROM   cddObservationFJoined obsf
             INNER JOIN cddBasePersonCohort pc
                     ON (obsf.patient_key = pc.patient_key)
        WHERE (pc.phr_flag = 'Y')

