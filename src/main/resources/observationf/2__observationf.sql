SELECT pers.patient_key          AS patient_key
                 ,o.observation_concept_id  AS observation_key
                 ,o.value_as_number         AS obs_value
                 ,o.unit_concept_id         AS unit_observation_key
                 ,o.observation_date        AS create_date
                 ,o.observation_date        AS obs_date
                 ,o.range_low               AS low_normal
                 ,o.range_high              AS high_normal
                 ,o.organization_id         AS cqr_organization_id
                 ,o.person_id               AS cqr_person_id
                 ,o.observation_id          AS cqr_observation_id
                 ,o.visit_occurrence_id     AS cqr_visit_occurrence_id
                 ,o.created_date            AS created_date
                 ,o.created_by              AS created_by
                 ,o.last_modified_date      AS last_modified_date
                 ,o.last_modified_by        AS last_modified_by
                 ,'24'                      AS CalcMonths
             FROM  cddPatientD pers
                   INNER JOIN (select * from omopObservation )  o
                           ON (o.organization_id   = pers.cqr_organization_id
                           AND o.person_id         = pers.cqr_person_id)
                           AND o.observation_type_concept_id in ('1131275','27309','27311','27316')

                WHERE (o.value_as_number is not null or LEN(RTRIM(LTRIM(o.value_as_number))) > 0 )
